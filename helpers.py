"""
Helper functions for Random Bot.
"""

import constants
import r6sapi as api

async def __signIn():
    """
    Function to sign the user into the r6s service.
    """
    user = api.Auth(email=constants.signInConstants["email"],
                    password=constants.signInConstants["password"],
                    max_connect_retries=10)

    return user

def __getPlatform(platform):
    """
    Function to get the platform from the api given user input.
    """
    if platform.lower() == "xbox":
        platform = api.Platforms.XBOX
    elif platform.lower() == "playstation":
        platform = api.Platforms.PLAYSTATION
    elif platform.lower() == "uplay":
        platform = api.Platforms.UPLAY
    else:
        platform = None
    
    return platform

def __getRankedRegion(region):
    """
    Function to get the ranked region from user input.
    """
    if region.lower() == "na":
        region = api.RankedRegions.NA
    elif region.lower() == "eu":
        region = api.RankedRegions.EU
    elif region.lower() == "asia":
        region = api.RankedRegions.ASIA
    else:
        region = None
    
    return region

def __getGameModeName(mode):
    """
    Function to get the gamemode name from user input.
    """
    if mode.lower() == "securearea":
        modeName = "securearea"
    elif mode.lower() == "bomb":
        modeName = "plantbomb"
    elif mode.lower() == "hostage":
        modeName = "rescuehostage"
    else:
        modeName = None
    
    return modeName

def __getWeaponID(weapon):
    """
    Function to get the weapon ID from user input.
    """
    if weapon.lower() == "ar":
        ID = api.WeaponTypes.ASSAULT_RIFLE
    elif weapon.lower() == "smg":
        ID = api.WeaponTypes.SUBMACHINE_GUN
    elif weapon.lower() == "shotgun":
        ID = api.WeaponTypes.SHOTGUN
    elif weapon.lower() == "dmr":
        ID = api.WeaponTypes.MACHINE_PISTOL
    elif weapon.lower() == "handgun":
        ID = api.WeaponTypes.HANDGUN
    elif weapon.lower() == "lmg":
        ID = api.WeaponTypes.LIGHT_MACHINE_GUN
    elif weapon.lower() == "mp":
        ID = api.WeaponTypes.MACHINE_PISTOL
    else:
        ID = None
        
    return ID

def __getPlayerStats(player):
    """
    Accesses the stats and returns them in a dict.
    """

    try:
        KDR = player.kills/player.deaths
    except ZeroDivisionError:
        KDR = "N/A"

    try:
        WLR = player.matches_won/player.matches_lost
    except ZeroDivisionError:
        WLR = "N/A"

    try:
        Accuracy = player.bullets_hit/player.bullets_fired
    except ZeroDivisionError:
        Accuracy = "N/A"

    try:
        HeadshotRate = player.headshots/player.bullets_hit
    except ZeroDivisionError:
        HeadshotRate = "N/A"

    # Handling errors in case of a ZeroDivisionError
    
    stats = {
        "Name": player.name,
        "Level": player.level,
        "Kills": player.kills,
        "Deaths": player.deaths,
        "KDR": KDR,
        "Assists": player.kill_assists,
        "Matches Won": player.matches_won,
        "Matches Lost": player.matches_lost,
        "WLR": WLR,
        "Time Played": player.time_played,
        "Bullets Fired": player.bullets_fired,
        "Bullets Hit": player.bullets_hit,
        "Headshots": player.headshots,
        "Accuracy": Accuracy,
        "Headshot Rate": HeadshotRate,
    }
    # Dict of stats

    return stats

def __getRankingStats(rank):
    """
    Same thing here but for ranking stats.
    """

    try:
        WLR = rank.wins/rank.losses
    except ZeroDivisionError:
        WLR = "N/A"

    stats = {
        "Current Rank": rank.rank,
        "Wins": rank.wins,
        "Losses": rank.losses,
        "Abandons": rank.abandons,
        "WLR": WLR,
        "Current MMR": rank.mmr,
        "MMR to Next Rank": rank.next_rank_mmr,
        "Season": rank.season,
        "Region": rank.region,
        "Mean Skill": rank.skill_mean
    }

    return stats

def __getOperatorStats(operator):
    """
    Same thing here but for operator stats.
    """
    
    try:
        WLR = operator.wins/operator.losses
    except ZeroDivisionError:
        WLR = "N/A"

    try:
        KDR = operator.kills/operator.deaths
    except ZeroDivisionError:
        KDR = "N/A"

    stats = {
        "Operator Name": operator.name.capitalize(),
        "Operator Wins": operator.wins,
        "Operator Losses": operator.losses,
        "Operator WLR": WLR,
        "Operator Kills": operator.kills,
        "Operator Deaths": operator.deaths,
        "Operator KDR": KDR,
        "Operator Headshots": operator.headshots,
        "Operator DBNO's": operator.dbnos,
        "Total XP": operator.xp,
        "Time Played": operator.time_played,
        operator.statistic_name: operator.statistic
    }

    return stats

def __getGameModeStats(mode):
    """
    Same thing here but for gamemode stats.
    """

    try:
        WLR = mode.won/mode.lost
    except ZeroDivisionError:
        WLR = "N/A"
    
    stats = {
        "Wins": mode.won,
        "Losses": mode.lost,
        "WLR": WLR,
        "Total Played": mode.played,
        "Best Score": mode.best_score
    }

    return stats

def __getQueueStats(queue):
    """
    Same thing here but for gamequeue stats.
    """

    try:
        WLR = queue.won/queue.lost
    except ZeroDivisionError:
        WLR = "N/A"

    try:
        KDR = queue.kills/queue.deaths
    except ZeroDivisionError:
        KDR = "N/A"
    
    stats = {
        "Wins": queue.won,
        "Losses": queue.lost,
        "WLR": WLR,
        "Games Played": queue.played,
        "Kills": queue.kills,
        "Deaths": queue.deaths,
        "KDR": KDR
    }

    return stats

def __getWeaponStats(weapon):
    """
    Same thing here but for weapon stats.
    """
    
    try:
        HeadshotRate = weapon.headshots/weapon.hits
    except ZeroDivisionError:
        HeadshotRate = "N/A"
        
    try:
        Accuracy = weapon.hits/weapon.shots
    except ZeroDivisionError:
        Accuracy = "N/A"

    stats = {
        "Name": weapon.name,
        "Kills": weapon.kills,
        "Headshots": weapon.headshots,
        "Hits": weapon.hits,
        "Shots Fired": weapon.shots,
        "Headshot Rate": HeadshotRate,
        "Accuracy": Accuracy
    }

    return stats
