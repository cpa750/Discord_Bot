"""
Simple discord bot.
Written using the discord.py rewrite by Cian Payne.

If you're seeing this, I want to know how you got into my GitLab account.
"""

import random

import helpers
# Helper functions and constants

import discord
from discord.ext import commands
from discord.ext.commands import Bot

bot = commands.Bot(command_prefix="$",
                   description="Random Bot")
# Instantiating the bot

@bot.event
async def on_ready():
    """
    Basic function to notify that the bot is ready.
    """
    print("Signed in as:")
    print("Name: "+bot.user.name)
    print("ID: "+str(bot.user.id))
    print("----------------------")
    
@bot.event
async def on_message(ctx):
    """
    Function called whenever any message is recieved.
    """
    message = ctx.content.lower()
    
    they_said_a_bad_word = False
    probability = 0
    
    for word in helpers.constants.BAD_WORDS:
        # Figuring out if they swore, then geting a probability to yell at them
        if word in message:
            they_said_a_bad_word = True
            if probability < 100:
                # Not allowing floats so 100 can't be divided by any number greater
                probability += 5
        
    if they_said_a_bad_word:
        rand1 = random.randint(0, 100/probability)
        rand2 = random.randint(0, 100/probability)

        if rand1 == rand2:
            await ctx.channel.send("{}, NO SWEARING AROUND MY GOOD CHRISIAN BOT!".format(ctx.author))
            
    if str(ctx.author) == "Bak#8703":
        await ctx.channel.send("Bak you can't use the bot mwahahahaha")
    else:
        await bot.process_commands(ctx)
    # Not letting Bak use the bot mwahahahahahaha

@bot.command(name="info", help="Info about this bot.")
async def info(ctx):
    """
    Basic info function.
    """
    embed = discord.Embed(title="Info")

    embed.add_field(name="Author", value="cpa750")
    embed.add_field(name="Other", value="Written using discord.py rewrite, implements the R6S API.")

    await ctx.send(embed=embed)

@bot.command(name="playchill", help="Gets a random chill music mix.",
             aliases=["music"])
async def __getChillMusic(ctx):
    """
    Function to get a random chillstep mix.
    """
    choice = random.choice(helpers.constants.MUSIC_URL_LIST)

    await ctx.send("Some chill music 4 u :musical_note:")
    await ctx.send(choice)
    
@bot.command(name="hello", help="Say hi!",
             aliases=["hi"])
async def __hello(ctx):
    """
    Says a personalized hello message.
    """
    await ctx.send("Hello there, {}!".format(ctx.message.author))

@bot.command(name="player", help="Get information about a specific player on a specific platform.")
async def __getPlayer(ctx, player, platform):
    """
    Gets the information from the API about a given player.
    """
    user = await helpers.__signIn()
    # getting the auth used to access the API

    platform = helpers.__getPlatform(platform)

    if platform is None:
        await ctx.send("Invalid platform")
        return
    
    try:
        player = await user.get_player(player, platform)
    except:
        await ctx.send("Invalid username")
        return

    await player.load_level()
    await player.load_general()
    # These must be called to load the general and level statistics
    
    playerStats = helpers.__getPlayerStats(player)
    # __getting the player stats from the external helper function
    
    embed = discord.Embed(title=player.name,
                          description="Player stats for {}".format(player.name))
    embed.set_image(url=player.icon_url)
    for statName in playerStats:
        # Adding each stat to the embed
        embed.add_field(name=statName, value=playerStats[statName], inline=True)

    await ctx.send(embed=embed)

@bot.command(name="rank", help="Get rank information about player on a specific platform and region.")
async def __getRank(ctx, player, platform, region, *args):
    """
    Gets ranking information about a given player in a given region.
    """
    user = await helpers.__signIn()

    platform = helpers.__getPlatform(platform)
    region = helpers.__getRankedRegion(region)

    if platform == None:
        await ctx.send("Invalid platform")
        return

    if region == None:
        await ctx.send("Invalid Region")
        return

    try:
        player = await user.get_player(player, platform)
    except:
        await ctx.send("Invalid username")
        return

    try:
        rankInfo = await player.get_rank(region=region, season=args[0])
        # If the optional season argument is given, use that
    except IndexError:
        rankInfo = await player.get_rank(region=region)
        # Otherwise, use the default

    stats = helpers.__getRankingStats(rankInfo)

    embed = discord.Embed(title="{} Ranking Information".format(player.name))
    for stat in stats:
        embed.add_field(name=stat, value=stats[stat], inline=True)

    await ctx.send(embed=embed)

@bot.command(name="operator", help="Get stats about a player's given operator.",
              aliases=["op"])
async def __getOperator(ctx, operator, player, platform):
    """
    Gets the stats about a certain player's given operator.
    """
    user = await helpers.__signIn()
    platform = helpers.__getPlatform(platform)

    if platform == None:
        await ctx.send("Invalid platform")
        return

    try:
        player = await user.get_player(player, platform)
    except:
        await ctx.send("Invalid username")
        return

    try:
        operator = await player.load_operator(operator.lower())
    except:
        await ctx.send("Invalid operator name")
        return

    stats = helpers.__getOperatorStats(operator)

    embed = discord.Embed(title="{}'s {} Statistics".format(player.name, operator.name.capitalize()))
    for stat in stats:
        embed.add_field(name=stat, value=stats[stat], inline=True)
    
    await ctx.send(embed=embed)

@bot.command(name="gamemode", help="Get stats about a player's performance on a given gamemode.",
             aliases=["mode"])
async def __getGameMode(ctx, mode, player, platform):
    """
    Gets the stats for a player's gamemode.
    """    
    user = await helpers.__signIn()

    platform = helpers.__getPlatform(platform)

    if platform == None:
        await ctx.send("Invalid platform")
        return

    try:
        player = await user.get_player(player, platform)
    except:
        await ctx.send("Invalid username")
        return

    modes = await player.check_gamemodes()

    modeName = helpers.__getGameModeName(mode)

    if modeName == None:
        await ctx.send("Invalid gamemode")
        return

    mode = modes[modeName]

    stats = helpers.__getGameModeStats(mode)

    embed = discord.Embed(title="{}'s {} Statistics".format(player.name, mode.name))
    for stat in stats:
        embed.add_field(name=stat, value=stats[stat], inline=True)
    
    await ctx.send(embed=embed)
    
@bot.command(name="gamequeue", help="Get stats about a player's performance on a specific game queue.",
             aliases=["queue", "q"])
async def __getGameQueue(ctx, queue, player, platform):
    """
    Gets the stats about a player's game queue.
    """
    user = await helpers.__signIn()

    platform = helpers.__getPlatform(platform)

    if platform == None:
        await ctx.send("Invalid platform")
        return

    try:
        player = await user.get_player(player, platform)
    except:
        await ctx.send("Invalid username")
        return

    await player.load_queues()
    
    if queue.lower() == "casual":
        queue = player.casual
    elif queue.lower() == "ranked":
        queue = player.ranked
    else:
        await ctx.send("Invalid queue")
        return
    
    stats = helpers.__getQueueStats(queue)

    embed = discord.Embed(title="{}'s {} Statistics".format(player.name, queue.name))
    for stat in stats:
        embed.add_field(name=stat, value=stats[stat], inline=True)
    
    await ctx.send(embed=embed)

@bot.command(name="weapon", help="Get stats about a player's given weapon.")
async def __getWeapon(ctx, weapon, player, platform):
    """
    Gets the stats about a certain player's given weapon.
    """
    user = await helpers.__signIn()

    platform = helpers.__getPlatform(platform)

    if platform == None:
        await ctx.send("Invalid platform")
        return

    try:
        player = await user.get_player(player, platform)
    except:
        await ctx.send("Invalid username")
        return

    weaponID = helpers.__getWeaponID(weapon)
    
    if weaponID == None:
        await ctx.send("Invalid weapon")
        return

    await player.load_weapons()
    
    weapon = player.weapons[weaponID]

    stats = helpers.__getWeaponStats(weapon)
    
    embed = discord.Embed(title="{}'s {} Statistics".format(player.name, weapon.name))
    for stat in stats:
        embed.add_field(name=stat, value=stats[stat])

    await ctx.send(embed=embed)

bot.run(helpers.constants.LOGIN_TOKEN)
# Running the bot using the super secret login token
